import React, { Component } from 'react';
import {
    View,
    Modal,
    StyleSheet,
    Button,
    Dimensions,
    TouchableHighlight,
    Text
} from 'react-native';

export default class Popup extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            number: 0
        }

        this.handlePress = this.handlePress.bind(this);
        this.handleModalPress = this.handleModalPress.bind(this);
    }

    handleModalPress() {
        this.setState({
            visible: true
        })
    }

    handlePress(num) {
        this.props.updateCats(num);
        this.setState({
            visible: false
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Button title={"settings"} onPress={this.handleModalPress} />
                <Modal transparent visible={this.state.visible} onDismiss={() => { this.setState({ visible: false })}} onRequestClose={() => {this.setState({ visible: false })}} >
                    <View style={styles.modal}>
                        <TouchableHighlight onPress={() => { this.handlePress(30) }} style={styles.button}>
                            <Text>30</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this.handlePress(50) }} style={styles.button}>
                            <Text>50</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this.handlePress(100) }} style={styles.button}>
                            <Text>100</Text>
                        </TouchableHighlight>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        top: 100,
        height: 200,
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    modal: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        height: 185,
        width: Dimensions.get('window').width,
        top: 0,
        backgroundColor: 'white'
    },
    button: {
        width: Dimensions.get('window').width / 4,
        marginLeft: Dimensions.get('window').width / 24,
        marginRight: Dimensions.get('window').width / 24,
        height: 100,
        top: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderColor: 'black',
        borderBottomWidth: 2,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderTopWidth: 2
    }
});