// https://www.youtube.com/watch?v=nuoP52d0kqI

import React, { Component } from "react";
import { Image, ActivityIndicator, View, Text, TouchableHighlight } from "react-native";

export default class CacheImage extends Component {

    state = {
        source: null,
        isLoading: true
    }

    componentDidMount() {
        this.setState({
            source: {
                uri: this.props.uri
            },
            isLoading: false
        })
    }

    handleClick() {
        this.props.screenProps.function(this.state.source, this.props.name);
        this.props.navigation.navigate('Card');
    };

    render() {
        return (
            (this.state.isLoading) ? (
                <ActivityIndicator size="large" color="#0000ff" />
            ) : (
                <TouchableHighlight onPress={this.handleClick.bind(this)}>
                    <View style={this.props.style.card}>
                        <Image style={this.props.style.image} source={this.state.source}/>
                        <Text style={this.props.style.text}>{this.props.number}. {this.props.name} the cat</Text>
                    </View>
                </TouchableHighlight>
            )
        );
    }
}
