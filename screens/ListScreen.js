import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Dimensions,
    ActivityIndicator,
    AsyncStorage,
    NetInfo
} from 'react-native';

import CacheImage from '../Components/CacheImage';
import Popup from "../Components/Popup";

export default class ListScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cards: [],
            isLoading: true
        }

        this.fetchNames = this.fetchNames.bind(this);
        this.saveCats = this.saveCats.bind(this);
        this.getCats = this.getCats.bind(this);
        this.updateCats = this.updateCats.bind(this);
        this.setStateCats = this.setStateCats.bind(this);
        this.handleConnectionChange = this.handleConnectionChange.bind(this);
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener("connectionChange", this.handleConnectionChange);
    }

    handleConnectionChange = (isConnected) => {
        if (isConnected) {
            let names = this.fetchNames(100);
            names.then(names => this.saveCats(names, 100))
                .then(() => this.getCats())
                .then((cats) => this.setStateCats(cats, 100));
        }
        else {
            alert("You are currently offline");
            let cats = this.getCats();
            cats.then(cats => this.setStateCats(cats, 100));
        }
    }
    
    fetchNames(num) {
        return fetch(`http://names.drycodes.com/${num}?nameOptions=boy_names&separator=space`)
            .then(res => res.json());
    }
    
    saveCats(names, num) {
        let KittensArr = [];
        for (let i = 0; i < num; i++) {
            let randomNum = Math.floor(Math.random() * 501) + 200;
            KittensArr.push({
                name: names[i],
                uri: `https://placekitten.com/${randomNum}/${randomNum}`
            });
        }
        AsyncStorage.setItem('Cats', JSON.stringify(KittensArr));
    }
    
    getCats() {
        return AsyncStorage.getItem('Cats')
            .then(res => {
                return JSON.parse(res);
            });
    }
    
    setStateCats(cats, num) {
        let cards = [];
        for (let i = 0; i < num; i++) {
            cards.push(<CacheImage
                {...this.props}
                key={i}
                number={i + 1}
                style={styles}
                uri={cats[i].uri}
                name={cats[i].name}
            />);
        };
        this.setState({
            cards,
            isLoading: false
        })
    }
        
    updateCats(num) {
        if (num <= this.state.cards.length) {
            this.setState({
                cards: this.state.cards.slice(0, num)
            })
        }
        else {
            let cats = this.getCats();
            cats.then(cats => this.setStateCats(cats, num));
        }
    }
    
    static navigationOptions = {
        header: null
    }
        
    render() {

        return(
            (this.state.isLoading) ? (
                <View style={styles.container}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : (
            <ScrollView>
                <View style={styles.container}>
                    <Popup 
                        updateCats={this.updateCats}
                        style={styles.container}
                    />
                    {this.state.cards}
                </View>
            </ScrollView>
            )
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    image: {
        height: Dimensions.get('window').width / 2,
        width: Dimensions.get('window').width / 2
    },
    card: {
        width: Dimensions.get('window').width / 1.5,
        height: Dimensions.get('window').height / 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
        margin: 10,
        borderRadius: 10
    },
    text: {
        color: 'white',
        margin: 5
    }
});