import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";

export default class KittenScreen extends Component { 

    render() {
        return (
        <View style={styles.container}>
            <Image style={styles.image} source={this.props.screenProps.uri}/>
            <Text style={styles.name}>{this.props.screenProps.name}</Text>
            <Text style={styles.lorem}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Nulla nec ligula eget diam lacinia porttitor quis et felis. Nam euismod, 
                neque vitae imperdiet feugiat, diam tortor pellentesque dui, sed elementum 
                orci erat nec tortor. Nulla eget eleifend ante. Fusce id vehicula orci. 
                Mauris sed est dui. Donec lorem justo, tincidunt et egestas sed, tempus vel 
                dolor. Suspendisse semper, mi eget iaculis blandit, magna tellus molestie mauris, 
                sed eleifend risus neque in dui. Donec gravida augue at mauris pellentesque, at 
                bibendum magna fermentum. In vulputate ligula nec lorem gravida tristique. Proin 
                non ex a est lobortis vehicula. Vivamus tempor blandit mauris vitae vulputate. 
                Sed ultrices, nunc at placerat consequat, dui odio elementum purus, ac maximus 
                leo ex eu sem.
            </Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center"
    },
    image: {
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').width,
    },
    lorem: {
        textAlign: 'center'
    },
    name: {
        fontSize: 18,
        fontWeight: 'bold',
        width: Dimensions.get('window').width,
        textAlign: 'center'
    }
});
