import React from 'react';
import { StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import ListScreen from './screens/ListScreen';
import KittenScreen from "./screens/KittenScreen";


export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.currentKitten = this.currentKitten.bind(this);

    this.state = {
      kitten: {
        uri: '',
        name: '',
        function: this.currentKitten
      }
    }
  }

  currentKitten(uri, name) {
    this.setState({
      kitten: {
        uri: uri,
        name: name,
        function: this.currentKitten
      }
    })
  }

  render() {

    return (
      <AppStackNavigator screenProps={this.state.kitten} />
    );
  }
}

const AppStackNavigator = createStackNavigator({
  List: ListScreen,
  Card: KittenScreen
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
